var TelegramBot = require("node-telegram-bot-api");
var config = require("./config")
var token = config.token
var bot = new TelegramBot(token, {polling:true});
var request = require("request")

var wolfram = require('./wolframalpha').createClient(config.wolfram_token)

canUse = config.canUse

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var titleToIncludeImage = {
  "Input interpretation":false,
  //Misc
  "Basic information":false,
  "Basic properties":false,
  "Members":false,
  "Transportation":false,
  "Media and telecommunications":false,
  "Education":false,
  "Dependencies":false,
  "Bordering countries/regions":false,
  "Geographic properties":false,
  "Name":false,
  "Largest cities":false,
  "Health care":false,
  "Business information":false,
  "Employment":false,
  "Product information":false,
  "Game properties":false,
  //People
  "Leadership position":false,
  "Physical characteristics":false,
  "Familial relationships":false,
  "Notable films":false,
  //Language
  "Inflected forms":false,
  "Synonyms":false,
  "Word origins":false,
  "Definitions":false,
  "Word frequency history":true,
  "Narrower terms":false,
  "Broader terms":false,
  "Rhymes":false,
  "Hyphenation":false,
  "Overall typical frequency":false,
  "Crossword puzzle clues":false,
  "Scrabble score":false,
  "Lexically close words":false,
  "Other notable uses":false,
  "Phrases":false,
  //websites
  "Web hosting information":false,
  //Finance
  "Currency conversions":false,
  //Blank
  "":true,
  //"":false,
}

function includeImage(title,subtitle,plaintext){
  if(plaintext == ""){return true;}
  if(titleToIncludeImage[title]!=null && titleToIncludeImage[title]!==undefined){return titleToIncludeImage[title];}
  if(title.match(/Webpage information for/)){return false;}
  if(title.match(/Web statistics for/)){return false;}
  if(title.match(/First known use/)){return false;} 
  if(title.match(/Additional currency conversions/)){return false;}
  if(title.indexOf("\"") > -1){return false;}
  return plaintext.indexOf("|") > -1;
}

function query(chatId,q){
  bot.sendMessage(chatId,"Working ...");
  wolfram.query(q, function(err, result) {
    if(err != null){console.log("error while handling query:",err);bot.sendMessage(chatId,"Looks like an error occoured ...\nplease contact bot author");return;}
    if(result.error != false){
      console.log("Wolframalpha returned an error",result.error,q);
      bot.sendMessage(chatId,"Wolframalpha returned an error on your query\n"+result.error.code+": "+result.error.msg)
      return;}
    var assumptions = result.assumptions
    if (assumptions!=null&&assumptions!==undefined){
      assumption_output = ""
      for(var i=0;i<assumptions.length;i++){
        var out = assumptions[i].type+":"+assumptions[i].word+" "+JSON.stringify(assumptions[i].values);
        assumption_output = assumption_output+"\n"+out;
      }
      if(assumption_output!=""){
        bot.sendMessage(chatId,"*> Assumptions:*"+assumption_output,{parse_mode:"Markdown"});
      }
    }
    if(!result.success){bot.sendMessage(chatId,"<No results>");return;}
    result = result.pods
    if(result==null||result===undefined){
      console.log("result.pods was empty for some reason");
      bot.sendMessage(chatId,"<No results>\n(result.pods didn't exist)\n¯\\_(ツ)_/¯");
      return;
    }
    for(var i=0;i<result.length;i++){
      console.log(result[i].title,result[i].subpods);
      for(var j=0;j<result[i].subpods.length;j++){
        if(result[i].subpods[j].plaintext == ""){bot.sendMessage(chatId,"*"+result[i].title+"*\n"+result[i].subpods[0].img.src,{parse_mode:"Markdown"});}
        else{
          subtitle = ""
          if (!(result[i].subpods[j].title == "")){subtitle = "_("+result[i].subpods[j].title+")_"}
          if (!includeImage(result[i].title,result[i].subpods[j].title,result[i].subpods[j].plaintext)){
            bot.sendMessage(chatId,"*"+result[i].title+"* "+subtitle+"\n"+result[i].subpods[j].plaintext,{parse_mode:"Markdown"});
          }else{
            bot.sendMessage(chatId,"*"+result[i].title+"* "+subtitle+"\n"+result[i].subpods[j].plaintext+"\n[Image]("+result[i].subpods[0].img.src+")",{parse_mode:"Markdown"});
          }
        }
      }
    }
  });
}

//no command (query)
bot.onText(/^([^\/].*)/,function(msg,match){
  if(!canUse(msg.from)){return;}
  var chatId = msg.chat.id;
  var q = match[1];
  query(chatId,q);
});

bot.onText(/^\/start/,function(msg,match){
  var chatId = msg.chat.id;
  if(canUse(msg.from)){bot.sendMessage(chatId,"Welcome to the wolframalpha bot!");}
  else{bot.sendMessage(chatId,"Sorry but this is a private bot of @Baschdel\nif you want the source code write him a nice message");}
});

bot.on('polling_error', (error) => {
  console.log("ERROR:",error);  // => 'EFATAL'
});
