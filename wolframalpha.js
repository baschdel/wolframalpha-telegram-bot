//baschdels personal edit

var request = require('request')

var Client = exports.Client = function Client(appKey) {
  this.appKey = appKey
}

Client.prototype.query = function(input, cb) {
  if(!this.appKey) {
    return cb("Application key not set", null)
  }

  var uri = 'http://api.wolframalpha.com/v2/query?output=JSON&units=metric&location=Wei%C3%9Fenburg%20in%20Bayern&input=' + encodeURIComponent(input) + '&primary=true&appid=' + this.appKey

  request(uri, function(error, response, body) {
    if(!error && response.statusCode == 200) {
      var root = JSON.parse(body)
      console.log(root)
      if(root.error) {
        var message = root.errormsg
        return cb(message, null)
      } else {
        return cb(null,root.queryresult)
      }
    } else {
      return cb(error, null)
    }
  })
}

exports.createClient = function(appKey) {
  return new Client(appKey)
}
